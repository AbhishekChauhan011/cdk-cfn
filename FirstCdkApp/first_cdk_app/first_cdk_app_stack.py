from aws_cdk import core as cdk
from aws_cdk import aws_dynamodb as dynamodb
from aws_cdk import aws_events as events
from aws_cdk import aws_sqs as sqs
from aws_cdk import aws_lambda as lambda_
from aws_cdk import aws_iam as iam
from aws_cdk import aws_lambda_event_sources as lambda_sources
from aws_cdk import aws_events_targets as targets
from aws_cdk import aws_lambda_destinations as lambda_destinations
from aws_cdk import core


class FirstCdkAppStack(cdk.Stack):

    def __init__(self, scope: cdk.Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # The code that defines your stack goes here
        eventPattern = events.EventPattern(source=["cdk-event-source"], detail_type=["cdk-event-detail-type"])

        my_table = dynamodb.Table(self, "table", table_name="cdk-table",
                    partition_key=dynamodb.Attribute(name="name", type=dynamodb.AttributeType.STRING),
        )


        my_event_bus = events.EventBus(self, "event_bus", event_bus_name="cdk-event-bus")

        my_event_bus2 = events.EventBus(self, "event_bus2", event_bus_name="cdk-event-bus2")

        my_sqs = sqs.Queue(self, "sqs", queue_name="cdk-queue")

        my_sqs2 = sqs.Queue(self, "sqs2", queue_name="cdk-queue2")

        my_rule = events.Rule(self, "rule", enabled=True, event_bus=my_event_bus, rule_name="cdk-rule",
                    event_pattern=eventPattern
        )

        my_rule.add_target(targets.SqsQueue(my_sqs))

        my_rule2 = events.Rule(self, "rule2", enabled=True, event_bus=my_event_bus2, rule_name="cdk-rule2",
                    event_pattern=eventPattern
        )

        my_rule2.add_target(targets.SqsQueue(my_sqs2))

        my_iam_role = iam.Role(self, "iam-role", role_name="cdk-role",
                 assumed_by=iam.ServicePrincipal("lambda.amazonaws.com")
        )

        my_iam_role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name('AmazonSQSFullAccess'))
        my_iam_role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name('AmazonEventBridgeFullAccess'))
        my_iam_role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name('service-role/AWSLambdaBasicExecutionRole'))
        my_iam_role.add_managed_policy(iam.ManagedPolicy.from_aws_managed_policy_name('service-role/AWSLambdaVPCAccessExecutionRole'))

        my_iam_role.add_to_policy(
                iam.PolicyStatement(
                effect=iam.Effect.ALLOW,
                resources=["*"],
                actions=[
                        "dynamodb:BatchGetItem",
                        "dynamodb:BatchWriteItem",
                        "dynamodb:TagResource",
                        "dynamodb:UntagResource",
                        "dynamodb:PutItem",
                        "dynamodb:DeleteItem",
                        "dynamodb:GetItem",
                        "dynamodb:Scan",
                        "dynamodb:Query",
                        "dynamodb:UpdateItem"
                    ]
                )
            )

        my_lambda = lambda_.Function(self, "lambda_function", function_name="cdk-lambda",
                    handler="index.lambda_handler",
                    runtime=lambda_.Runtime.PYTHON_3_7,
                    code = lambda_.Code.from_asset("resources"),
                    environment=dict(tableName=my_table.table_name, eventBusName=my_event_bus2.event_bus_name),
                    role=my_iam_role
        )

        my_lambda.add_event_source(lambda_sources.SqsEventSource(my_sqs))

        my_lambda2 = lambda_.Function(self, "lambda_function2", function_name="cdk-lambda2",
                    handler="index2.lambda_handler",
                    runtime=lambda_.Runtime.PYTHON_3_7,
                    code = lambda_.Code.from_asset("resources"),
                    environment=dict(webHookUrl="https://nagarro.webhook.office.com/webhookb2/7b503bc0-3184-41f2-8537-b20aacdd9375@a45fe71a-f480-4e42-ad5e-aff33165aa35/IncomingWebhook/cc9b37df2e5141488ad5468d65f7caf5/70997456-c131-4c54-9b30-9506c246e149"),
                    role=my_iam_role
        )

        my_lambda2.add_event_source(lambda_sources.SqsEventSource(my_sqs2))
