import json
import os
import logging
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event, context):
    print("Hello from the lambda 2 function")
    data = event['Records'][0]
    
    body = data['body']
    body = json.loads(body)
    
    detail = body["detail"]
    
    name = detail["name"]
    
    teams_message = {
        "@context": "https://schema.org/extensions",
        "@type": "MessageCard",
        "themeColor": "64a837",
        "title": "Dynamo DB Notification",
        "text": f"Data - {name}  successfully inserted into dynamodb ",
    }
    
    request = Request(
        "https://nagarro.webhook.office.com/webhookb2/7b503bc0-3184-41f2-8537-b20aacdd9375@a45fe71a-f480-4e42-ad5e-aff33165aa35/IncomingWebhook/cc9b37df2e5141488ad5468d65f7caf5/70997456-c131-4c54-9b30-9506c246e149",
        json.dumps(teams_message).encode('utf-8'))

    try:
        response = urlopen(request)
        response.read()
        logger.info("Message posted")
    except HTTPError as err:
        logger.error(f"Request failed: {err.code} {err.reason}")
    except URLError as err:
        logger.error(f"Server connection failed: {err.reason}")