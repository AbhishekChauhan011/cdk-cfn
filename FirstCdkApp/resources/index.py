import json
import os
import boto3

def lambda_handler(event, context):
    dynamodb = boto3.resource('dynamodb')
    
    new_event = event['Records']
    data = new_event[0]
    
    body = data['body']
    body = json.loads(body)
    
    source = body["source"]
    detail_type = body["detail-type"]
    detail = body["detail"]
    
    name = detail["name"]
    
    tableName = os.environ["tableName"]
    table = dynamodb.Table(tableName)
    
    response = table.put_item(
    TableName= tableName,    
    Item={'name': name}
    )
    
    client = boto3.client('events')
    
    response = client.put_events(
    Entries=[
                {
                    'Source': source,
                    'Resources': [],
                    'DetailType': detail_type,
                    'Detail': json.dumps(detail),
                    'EventBusName': os.environ["eventBusName"]
                },
            ]
        )
    
    return response
